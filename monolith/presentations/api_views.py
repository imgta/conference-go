from django.http import JsonResponse
from .models import Presentation
from events.models import Conference
from events.api_views import ConferenceListEncoder
from django.forms.models import model_to_dict
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
from .producer import approve, reject
import json


class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
        "conference",
        ]
    encoders = {"conference": ConferenceListEncoder()}

    def get_extra_data(self, obj):
        return {"status": obj.status.name}


class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = ["title", "company_name", "conference", ]
    encoders = {"conference": ConferenceListEncoder()}

    def get_extra_data(self, obj):
        return {"status": obj.status.name}


@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    if request.method == "GET":
        presentation = Presentation.objects.filter(conference=conference_id)
        return JsonResponse(
            {"presentations": presentation},
            encoder=PresentationListEncoder,
            safe=False,
            )
    else:
        content = json.loads(request.body)
        try:
            conference = Conference.objects.get(id=conference_id)
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )
        presentation = Presentation.create(**content)
        return JsonResponse(
            presentation, encoder=PresentationListEncoder, safe=False
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_presentation(request, id):
    if request.method == "GET":
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(presentation,
                            encoder=PresentationDetailEncoder,
                            safe=False
                            )
    elif request.method == "DELETE":
        count, _ = Presentation.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "conference" in content:
                conference = Conference.objects.get(id=id)
                content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )
    Presentation.objects.filter(id=id).update(**content)
    presentation = Presentation.objects.get(id=id)
    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )


@require_http_methods(["PUT"])
def api_approve_presentation(request, id):
    presentation = Presentation.objects.get(id=id)
    presentation.approve()
    body = json.dumps(
        {
            "presenter_name": presentation.presenter_name,
            "presenter_email": presentation.presenter_email,
            "title": presentation.title,
        }
    )
    approve(body)
    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )


@require_http_methods(["PUT"])
def api_reject_presentation(request, id):
    presentation = Presentation.objects.get(id=id)
    presentation.reject()
    body = json.dumps(
        {
            "presenter_name": presentation.presenter_name,
            "presenter_email": presentation.presenter_email,
            "title": presentation.title,
        }
    )
    reject(body)
    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )


    # present_dict = model_to_dict(presentation)
    # conference = {
    #         "name": presentation.conference.name,
    #         "href": presentation.conference.get_api_url(),
    #     }
    # present_dict['conference'] = conference
    # return JsonResponse(present_dict)

    # response = {
    #     key: val for key, val in presentation.__dict__.items() if not key.startswith('_')
    # }
    # response['conference'] = conference

    # presentation = model_to_dict(Presentation.objects.get(id=id))
    # presentation.pop('id')
    # conf = Conference.objects.get(id=presentation['conference'])
    # presentation['conference'] = {'name': conf.name, 'href': conf.get_api_url()}
    # return JsonResponse(presentation)
