from json import JSONEncoder
from datetime import datetime
from django.db.models import QuerySet


# Take date and convert to string to make it serializable
class DateEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime):
            return obj.isoformat()
        return super().default(obj)


class QuerySetEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, QuerySet):
            return list(obj)
        return super().default(obj)


class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):
    encoders = {}

    def default(self, obj):
        if isinstance(obj, self.model):
            dict = {
                property: self.encoders[property].default(getattr(obj, property))
                if property in self.encoders else getattr(obj, property)
                for property in self.properties
            }
            dict.update(self.get_extra_data(obj))
            if hasattr(obj, "get_api_url"):
                dict["href"] = obj.get_api_url()
            return dict
        return super().default(obj)

    def get_extra_data(self, obj):
        return {}
