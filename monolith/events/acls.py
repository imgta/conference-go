import requests, json, os
from dotenv import load_dotenv
from .keys import PEXEL_API_KEY, OPENWEATHER_API_KEY

# load_dotenv()
# WEATHER_KEY = os.getenv('OPENWEATHER_API_KEY')
# PICTURE_KEY = os.getenv('PEXEL_API_KEY')


def weather_data(city, state):
# United States ISO code is 3166-2
    ISO = str(3166-2)

# Create the URL for the geocoding API with the city and state
    BASE_URL = "https://api.openweathermap.org/data/2.5/weather"
    params = {
        "q": f"{city},{state},{ISO}",
        "appid": OPENWEATHER_API_KEY
    }
# URL = f"{BASE_URL}q={city},{state},{ISO}&appid={OPENWEATHER_API_KEY}"

# HTTP request, check status code of request
    response = requests.get(BASE_URL, params=params)
    location = json.loads(response.content)

# Get the latitude and longitude from the response
    lat = location["coord"]["lat"]
    lon = location["coord"]["lon"]

# Create the URL for the current weather API with the latitude and longitude
    POINT_URL = "https://api.openweathermap.org/data/2.5/weather"
    params = {
        "lat": lat,
        "lon": lon,
        "appid": OPENWEATHER_API_KEY,
        "units": "imperial"
    }
# POINT_URL = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPENWEATHER_API_KEY}&units=imperial"

# Second HTTP request
    response2 = requests.get(POINT_URL, params=params)
    area = json.loads(response2.content)

# Curate weather parameters here
    info = area["weather"][0]["description"]
    temp = area["main"]["temp"]
    feels = area["main"]["feels_like"]
    weather = {
        "description": info,
        "temperature": temp,
        "feelslike": feels,
    }
    try:
        return weather
    except (KeyError, IndexError):
        return {
            "description": None,
            "temperature": None,
            "feelslike": None,
            }


def get_pic(city, state):
# Create a dictionary for the headers to use in the request
    headers = {"Authorization": PEXEL_API_KEY}
    params = {
        "query": f"{city} {state}",
        "size": "original",
    }
# Create the URL for the request with the city and state
    BASE_URL = "https://api.pexels.com/v1/search"
# Make response and parse JSON
    response = requests.get(BASE_URL, headers=headers, params=params)
    content = json.loads(response.content)
# Return a dictionary that contains a `picture_url` key and one of the URLs for one of the pictures in the response
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}
