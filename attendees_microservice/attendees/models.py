from django.db import models
from django.urls import reverse
# from django.core.exceptions import ObjectDoesNotExist


class AccountVO(models.Model):
    email = models.EmailField()
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    is_active = models.BooleanField(default=True)
    updated = models.DateTimeField(auto_now_add=True)


class ConferenceVO(models.Model):
    name = models.CharField(max_length=200)
    import_href = models.CharField(max_length=75, unique=True)
    # "/api/conferences/1/"


# The Attendee model represents someone that wants to attend a conference
class Attendee(models.Model):
    email = models.EmailField()
    name = models.CharField(max_length=200)
    company_name = models.CharField(max_length=200, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    conference = models.ForeignKey(
        ConferenceVO,
        related_name="attendees",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("api_show_attendee", kwargs={"id": self.id})

    def create_badge(self):
        if not self.badge_id:
            self.badge = Badge.objects.create(attendee=self)
            self.save()


# Badge model represents the badge an attendee wears at the conference.
# Badge is a VO, therefore, does not have direct URL to view it.
class Badge(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    attendee = models.OneToOneField(
        Attendee,
        related_name="badge",
        on_delete=models.CASCADE,
        primary_key=True,
    )
